import { f } from '../src/js/module'

test('f should return 42', () => {
    expect(f()).toBe(42)
})
