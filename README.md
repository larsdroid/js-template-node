# js-template-node

A template to be used as a **JavaScript Node application**.  

## Includes

* NPM configuration (basically, the contents of our old trusty `package.json`)
* Jest setup (tests are executed using `npm run test`)
* ESLint (configured in `.eslintrc.json`)
  * Recommended VSCode setup is to use the ESLint plugin with these (global or project-specific) settings:
    ```
    ...
    "editor.codeActionsOnSave": {
        "source.organizeImports": true,
        "source.fixAll.eslint": true,
        "source.fixAll": true,
    },
    ...
    ```
